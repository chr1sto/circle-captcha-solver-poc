﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenCVTest
{
    public class CaptchaSolvedEntry
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Param2 { get; set; }
        public int Radius { get; set; }
        public Guid Id { get; set; }
    }
}
