﻿using Newtonsoft.Json;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OpenCVTest
{
    public partial class Form1 : Form
    {
        private string captcha = @"C:\Users\Christo\Desktop\captcha.png";

        private int Successfull = 0;
        private int Failed = 0;

        private Bitmap CurrentCaptcha { get; set; }

        private int X = 0;
        private int Y = 0;
        private int Radius = 0;
        private int param2 = 0;

        public Form1()
        {
            InitializeComponent();
            this.pictureBox1.Image = Image.FromFile(captcha);
            this.textBox1.Text = "1";
            this.textBox2.Text = "10";
            this.textBox3.Text = "190";
            this.textBox4.Text = "25";
            this.textBox5.Text = "15";
            this.textBox6.Text = "50";
            //Test();
        }

        private void Test()
        {

            var color = Cv2.ImRead(captcha, ImreadModes.Color);
            var color2 = new Mat();
            color.CopyTo(color2);
            var gray = color.CvtColor(ColorConversionCodes.RGBA2GRAY);

            Mat canny = new Mat();
            Cv2.Canny(gray, canny, 200, 20);

            int.TryParse(this.textBox1.Text, out int dp);
            int.TryParse(this.textBox2.Text, out int minDist);
            int.TryParse(this.textBox3.Text, out int param1);
            int.TryParse(this.textBox4.Text, out int param2);
            int.TryParse(this.textBox5.Text, out int minRadius);
            int.TryParse(this.textBox6.Text, out int maxRadius);

            bool hasOneOpen = false;

            while(!hasOneOpen && param2 > 0)
            {
                this.textBox4.Text = param2.ToString();

                color2.CopyTo(color);
                var circles = gray.HoughCircles(HoughMethods.Gradient, dp, minDist, param1, param2, minRadius, maxRadius);
                if (circles != null && circles.Length > 2)
                {
                    Mat dt = new Mat();

                    Cv2.DistanceTransform(255 - canny.GreaterThan(0), dt, DistanceTypes.L2, DistanceMaskSize.Mask3);

                    float mostOpen = 100.0f;
                    float minInlierDist = 2.0f;
                    //check for open circles
                    CircleSegment circleToClick = new CircleSegment();
                    circleToClick.Radius = 0;
                    foreach (var circle in circles)
                    {
                        if (!(circle.Center.X + circle.Radius > gray.Width
                            || circle.Center.X - circle.Radius < 0.0f
                            || circle.Center.Y + circle.Radius > gray.Height - 15
                            || circle.Center.Y - circle.Radius < 0.0f))
                        {
                            int counter = 0;
                            int inlier = 0;

                            float maxInlierDist = circle.Radius / 25.0f;
                            if (maxInlierDist < minInlierDist) maxInlierDist = minInlierDist;

                            float avgThicknessSum = 0.0f;

                            for (float t = 0; t < 2 * 3.14159265359f; t += 0.1f)
                            {
                                counter++;
                                float cY = Math.Min(circle.Radius * (float)Math.Sin(t) + circle.Center.Y, dt.Height - 1);
                                float cX = Math.Min(circle.Radius * (float)Math.Cos(t) + circle.Center.X, dt.Width - 1);

                                var at = dt.At<float>((int)Math.Round(cY), (int)Math.Round(cX));
                                if (at < maxInlierDist)
                                {
                                    var currCol = color2.At<ushort>((int)Math.Round(cY), (int)Math.Round(cX));
                                    int thickness = GetOutlineThickness((int)Math.Round(circle.Radius), 1, circle.Center.X, circle.Center.Y, color2, t, currCol);
                                    thickness = GetOutlineThickness((int)Math.Round(circle.Radius), -1, circle.Center.X, circle.Center.Y, color2, t, currCol, thickness);
                                    avgThicknessSum += thickness;
                                    inlier++;
                                }
                                else
                                {
                                    color.Circle(new OpenCvSharp.Point((int)cX, (int)cY), 2, Scalar.Blue);
                                }
                            }

                            var openPercent = 100.0f * inlier / counter;
                            if (openPercent > 70.0f && openPercent < 92.0f && openPercent < mostOpen && (avgThicknessSum / counter) > 0.9f)
                            {
                                mostOpen = openPercent;
                                mostOpen = openPercent;
                                mostOpen = openPercent;
                                mostOpen = openPercent;
                                circleToClick = circle;
                            }

                            Console.WriteLine($" Circle at {circle.Center.X}:{circle.Center.Y}");
                            Console.WriteLine($" Average Thickness: {avgThicknessSum / counter}");
                            Console.WriteLine($"{100.0f * inlier / counter} % of a circle");
                            Console.WriteLine("----------------------------------------------");
                            color.Circle(new OpenCvSharp.Point((int)circle.Center.X, (int)circle.Center.Y), (int)circle.Radius, Scalar.Red, 1);
                        }
                    }
                    if (mostOpen != 100.0f)
                    {
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.WriteLine("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV");
                        Console.WriteLine("            Found Open Circle!                ");
                        Console.WriteLine("______________________________________________");
                        Console.WriteLine("-> X: " + circleToClick.Center.X.ToString());
                        Console.WriteLine("-> Y: " + circleToClick.Center.Y.ToString());
                        Console.WriteLine("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
                        Console.ForegroundColor = ConsoleColor.White;
                        this.label9.Text = circleToClick.Center.X.ToString();
                        this.label10.Text = circleToClick.Center.Y.ToString();
                        color.Circle(new OpenCvSharp.Point((int)circleToClick.Center.X, (int)circleToClick.Center.Y), 4, Scalar.Orange, 4);
                        hasOneOpen = true;
                    }
                    else
                    {
                        param2--;
                    }
                    Console.WriteLine("##############################################");
                    Console.WriteLine("##############################################");
                    Console.WriteLine("##############################################");
                }
                else
                {
                    param2--;
                }
            }

            this.textBox4.Text = "25";
            this.pictureBox2.Image = OpenCvSharp.Extensions.BitmapConverter.ToBitmap(color);

            if (hasOneOpen)
            {

            }
        }

        private int GetOutlineThickness(int radius, int dir, float posX, float posY, Mat coloredMat, float w, ushort color, int thickness = 0)
        {
            radius = radius + dir;
            float dY = Math.Min(radius * (float)Math.Sin(w) + posY, coloredMat.Height - 1);
            float dX = Math.Min(radius * (float)Math.Cos(w) + posX, coloredMat.Width - 1);

            if(dY > coloredMat.Height - 18 || dY < 0 || dX > coloredMat.Width || dX < 0)
            {
                return thickness;
            }

            ushort colorAt = coloredMat.At<ushort>((int)Math.Round(dY), (int)Math.Round(dX));
            if(colorAt == color)
            {
                return GetOutlineThickness(radius, dir, posX, posY, coloredMat, w, color, thickness+1);
            }
            else
            {
                return thickness;
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Test();
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Failed++;
            this.toolStripStatusLabel4.Text = Failed.ToString();
            var percent = 100.0d * Successfull / (Successfull + Failed);
            this.toolStripStatusLabel6.Text = $"{percent} %";
            var id = Guid.NewGuid();
            this.CurrentCaptcha.Save(@"C:\temp\c1\failed\" + id + ".png");
            LoadNewCaptcha();
            Test();
        }

        private void LoadNewCaptcha()
        {
            var webClient = new WebClient();
            var stream = webClient.OpenRead(@"http://engine10.earthlost.de/captcha.php?id=190319076&sid=2mveu2b7903o1obmol95817g37");
            this.CurrentCaptcha = new Bitmap(stream);
            this.captcha = @"C:\temp\captchas\" + Guid.NewGuid() + ".png";
            this.CurrentCaptcha.Save(captcha, ImageFormat.Png);
            this.pictureBox1.Image = this.CurrentCaptcha;
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            LoadNewCaptcha();
            Test();
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            Successfull++;
            var percent = 100.0d * Successfull / (Successfull + Failed);
            this.toolStripStatusLabel6.Text = $"{percent} %";
            this.toolStripStatusLabel2.Text = Successfull.ToString();
            var id = Guid.NewGuid();
            var result = new CaptchaSolvedEntry()
            {
                X = this.X,
                Y = this.Y,
                Param2 = this.param2,
                Radius = this.Radius,
                Id = id
            };


            List<CaptchaSolvedEntry> entries;
            if (File.Exists(@"C:\temp\c1\list.txt"))
            {
                var text = File.ReadAllText(@"C:\temp\c1\list.txt");
                entries = JsonConvert.DeserializeObject<List<CaptchaSolvedEntry>>(text);
            }
            else
            {
                entries = new List<CaptchaSolvedEntry>();
            }

            entries.Add(result);

            File.WriteAllText(@"C:\temp\c1\list.txt", JsonConvert.SerializeObject(entries));

            this.CurrentCaptcha.Save(@"C:\temp\c1\success\" + id + ".png");
            LoadNewCaptcha();
            Test();
        }
    }
}
