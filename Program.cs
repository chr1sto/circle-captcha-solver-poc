﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OpenCVTest
{
    static class Program
    {
        private static string captcha = @"C:\Users\Christo\Desktop\captcha.png";

        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        //[STAThread]
        static void Main()
        {
            showImage = CreateNonIndexedImage(Image.FromFile(captcha));
            //Test3();
            Test2();
        }

        private static Bitmap showImage;

        public static Bitmap CreateNonIndexedImage(Image src)
        {
            Bitmap newBmp = new Bitmap(src.Width, src.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            using (Graphics gfx = Graphics.FromImage(newBmp))
            {
                gfx.DrawImage(src, 0, 0);
            }

            return newBmp;
        }


        /*public struct Pixel
        {
            public Color Color;
            public int X;
            public int Y;

            public Pixel(Color color, int x, int y)
            {
                Color = color;
                X = x;
                Y = y;
            }
        }

        private static int maxDepth = 1000;
        private static void Test3()
        {
            Bitmap bitmap = CreateNonIndexedImage(Image.FromFile(captcha));
            for(int x = 0; x < bitmap.Width; x++)
            {
                for(int y = 0; y < bitmap.Height - 18; y++)
                {
                    var color = bitmap.GetPixel(x, y);
                    Pixel pixel = new Pixel(color, x, y);
                    if (!checkedPixels.Any(e => e.X == pixel.X && e.Y == pixel.Y))
                    {
                        var adjPixs = new List<Pixel>();
                        GetAdjecantPixelsWithSameColor(ref bitmap, ref pixel, ref adjPixs);
                        if (adjPixs.Count > 80)
                        {
                            foreach (var item in adjPixs)
                            {
                                showImage.SetPixel(item.X, item.Y, Color.Red);
                            }
                            Console.WriteLine(adjPixs.Count());
                        }
                    }
                }
            }
            showImage.Save(@"C:\temp\c2\haha.png", ImageFormat.Png);
            Console.ReadKey();
        }

        private static void GetAdjecantPixelsWithSameColor(ref Bitmap bitmap, ref Pixel origin, ref List<Pixel> adjecantPixels, int depth = 0)
        {
            checkedPixels.Add(origin);
            adjecantPixels.Add(origin);
            if (depth == maxDepth) return;
            CheckPixel(0, -1,ref bitmap,ref origin,ref adjecantPixels, depth);
            CheckPixel(1, -1, ref bitmap, ref origin, ref adjecantPixels, depth);
            CheckPixel(1, 0, ref bitmap, ref origin, ref adjecantPixels, depth);
            CheckPixel(1, 1, ref bitmap, ref origin, ref adjecantPixels, depth);
            CheckPixel(0, 1, ref bitmap, ref origin, ref adjecantPixels, depth);
            CheckPixel(-1, 1, ref bitmap, ref origin, ref adjecantPixels, depth);
            CheckPixel(-1, 0, ref bitmap, ref origin, ref adjecantPixels, depth);
            CheckPixel(-1, -1, ref bitmap, ref origin, ref adjecantPixels, depth);
        }

        private static List<Pixel> checkedPixels = new List<Pixel>();
        private static void CheckPixel(int x, int y,ref Bitmap bitmap, ref Pixel origin, ref List<Pixel> adjecantPixels, int depth)
        {
            int x2 = origin.X + x;
            int y2 = origin.Y + y;
            if (x2 >= bitmap.Width || x2 < 0 || y2 >= bitmap.Height || y2 < 0) return;
            var color = bitmap.GetPixel(x2, y2);
            Pixel pixel = new Pixel(color, x2, y2);
            if(pixel.Color == origin.Color && !adjecantPixels.Any(e => e.X == pixel.X && e.Y == pixel.Y) && !checkedPixels.Any(e => e.X == pixel.X && e.Y == pixel.Y))
            {
                checkedPixels.Add(pixel);
                adjecantPixels.Add(pixel);
                GetAdjecantPixelsWithSameColor(ref bitmap, ref pixel, ref adjecantPixels,depth+1);
            }
        }
        */
        
        private static void Test2()
        {
            //iterator over ever pixel
            //neighbooringpixels
            Bitmap bitmap = new Bitmap(captcha);
            Rectangle rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            BitmapData bmpData = bitmap.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            IntPtr ptr = bmpData.Scan0;
            int bytes = bmpData.Stride * (bitmap.Height);
            byte[] rgbValues = new byte[bytes];

            Marshal.Copy(ptr, rgbValues, 0, bytes);

            int stride = bmpData.Stride;

            for (int column = 0; column < bmpData.Width; column++)
            {
                for (int row = 0; row < bmpData.Height-18; row++)
                {
                    var pixel = GetPixel(column, row, ref rgbValues, stride);
                    var adjecantPixels = new HashSet<Pixel>();
                    var pix = (Pixel)pixel;
                    if(!checkedPixels.Any(e => e.X == pix.X && e.Y == pix.Y))
                    {
                        GetNeighBoouringPixels(ref rgbValues, ref pix, ref adjecantPixels, stride, bmpData.Width, bmpData.Height - 18);
                        if (adjecantPixels.Count > 80)
                        {
                            /*
                            foreach(var item in adjecantPixels)
                            {
                                showImage.SetPixel(item.X, item.Y, Color.Red);
                            }
                            */
                            Console.WriteLine($"col::{column}::{adjecantPixels.Count()}");
                        }
                    }
                }
            }
            Console.ReadKey();
            showImage.Save(@"C:\temp\c2\haha.png", ImageFormat.Png);
        }

        public struct Pixel
        {
            public Pixel(int x, int y, byte r, byte g, byte b)
            {
                X = x;
                Y = y;
                R = r;
                G = g;
                B = b;
            }

            public int X;
            public int Y;
            public byte R;
            public byte G;
            public byte B;
        }

        private static HashSet<Pixel> checkedPixels = new HashSet<Pixel>();
        private static void GetNeighBoouringPixels(ref byte[] rgb, ref Pixel origin, ref HashSet<Pixel> adjecantPixels, int stride, int xMax, int yMax)
        {
            adjecantPixels.Add(origin);
            checkedPixels.Add(origin);
            CheckPixel(ref origin,0, -1,ref rgb,ref adjecantPixels,stride,xMax,yMax);
            CheckPixel(ref origin, 0, 1, ref rgb, ref adjecantPixels, stride, xMax, yMax);
            CheckPixel(ref origin, 1, -1, ref rgb, ref adjecantPixels, stride, xMax, yMax);
            CheckPixel(ref origin, 1, 0, ref rgb, ref adjecantPixels, stride, xMax, yMax);
            CheckPixel(ref origin, 1, 1, ref rgb, ref adjecantPixels, stride, xMax, yMax);
            CheckPixel(ref origin, -1, -1, ref rgb, ref adjecantPixels, stride, xMax, yMax);
            CheckPixel(ref origin, -1, 0, ref rgb, ref adjecantPixels, stride, xMax, yMax);
            CheckPixel(ref origin, -1, 1, ref rgb, ref adjecantPixels, stride, xMax, yMax);
        }

        private static void CheckPixel(ref Pixel origin,int dx, int dy, ref byte[] rgb, ref HashSet<Pixel> adjecantPixels, int stride, int xMax, int yMax)
        {
            int x = origin.X + dx;
            int y = origin.Y + dy;
            if (x >= xMax || x < 0 || y >= yMax || y < 0) return;
            var pixel = GetPixel(x, y, ref rgb, stride);
            //Der Shit zieht Performance!!!
            if (pixel.R == origin.R && pixel.G == origin.G && pixel.B == origin.B && !checkedPixels.Contains(pixel))
            {
                checkedPixels.Add(pixel);
                adjecantPixels.Add(pixel);
                GetNeighBoouringPixels(ref rgb, ref pixel, ref adjecantPixels, stride, xMax, yMax);
            }
        }

        private static Pixel GetPixel(int x, int y, ref byte[] rgb, int stride)
        {
            return new Pixel(x, y, (byte)(rgb[(x * stride) + (y * 3)]), (byte)(rgb[(x * stride) + (y * 3) + 1]), (byte)(rgb[(x * stride) + (y * 3) + 2]));
        }

    }
}
